import React, {useState, useEffect} from 'react';
import * as ReactBootStrap from 'react-bootstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.css';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
import { filter } from 'minimatch';
import ToolkitProvider, { CSVExport } from 'react-bootstrap-table2-toolkit';

function DataList(){
    const [userList, setUserList] = useState([]);

    const { ExportCSVButton } = CSVExport;

    const MyExportCSV = (props) => {
        const handleClick = () => {
            props.onExport();
        };
        return (
            <div>
                <button className="btn btn-warning" onClick={handleClick}>Export to CSV</button>
            </div>
        );
    };

    const columns = [
        {dataField:'id', text: 'Id'},
        {dataField:'index', text: 'Index', sort: true, filter: textFilter()},
        {dataField:'country', text: 'Country', sort: true, filter: textFilter()},
        {dataField:'value', text: 'Value', sort: true, filter: textFilter()},
        {dataField:'store', text: 'Store', sort: true, filter: textFilter()},
        {dataField:'returned', text: 'Returned', sort: true, filter: textFilter()}
    ]
     const pagination = paginationFactory({
         page: 1,
         sizePerPage: 5,
         lastPageText: '>>',
         firstPageText: '<<',
         nextPageText: '>',
         prePageText: '<',
         showTotal: true,
         alwaysShowAllBtns: true,
         onPageChange: function (page, sizePerPage){
             console.log('page', page);
             console.log('sizePerPage', sizePerPage);
         },
         onSizePerPageChange: function (page, sizePerPage){
             console.log('page', page);
             console.log('sizePerPage', sizePerPage);
         }
     });
  
    useEffect(() => {
      fetch('http://www.mocky.io/v2/5d4caeb23100000a02a95477')
      .then(response => response.json())
      .then(result => setUserList(result))
      .catch(error => console.log(error));
    },[])
      
    return (
      <div>
        <ToolkitProvider
            bootstrap4
            keyField
            data={userList}
            columns={columns}
            exportCSV
        >
              {
                  props => (
                    <React.Fragment>
                      <MyExportCSV {...props.csvProps}/>
                        <BootstrapTable striped bordered hover responsive dark
                        // bootstrap4 
                        // keyField='id' 
                        // columns={columns} 
                        // data={userList} 
                        pagination={pagination}
                        filter={filterFactory()}
                        {...props.baseProps}
                        />
                    </React.Fragment>
                )
            }
        </ToolkitProvider>
            
            
            {/* <table>
                <tr>
                    <th>Id</th>
                        <th>Index</th>
                        <th>Country</th>
                        <th>Value</th>
                        <th>Store</th>
                        <th>Returned</th>
                </tr>
                    {
                        userList && userList.length> 0 ?
                        userList.map(usr =>
                        <tr>
                            <td>{usr.id}</td>
                            <td>{usr.index}</td>
                            <td>{usr.country}</td>
                            <td>{usr.value}</td>
                            <td>{usr.store}</td>
                            <td>{usr.returned}</td>
                        </tr>
                        )
                        :'Loading'               
                        }
            </table> */}
      </div>
    );
  }

export default DataList;