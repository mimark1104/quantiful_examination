Task
Display the set of sales transactions to the user. Create a table that shows all the
sales transactions made by each store. The table should be dynamic and can ideally
be filtered by certain fields (such as by country and store).
It would be ideal to:

● see only sales values that are not returned
● view sales by country
● view the sales in different currencies
● see the sales by stores
● write a brief summary of your findings and how you approached the task


-use hooks
-use useState and useEffect
-installed bootstrap
-install react-bootstrap-table-next(BootstrapTable)
-install react-bootstrap-pagination(paginationFactory) added pagination for easy searching.
-install react-bootstrap-table2-filter(filterFactory)
-sorting features on the table(except "ID")
-added onClick event for CSV export button feature

//"see the sales by stores" is not implemented yet.